# The Smart Design test Chat Application #

### Tech stack ###

Spring + Docker + RabbitMQ + WebSocket + Maven

### Running the application ###

* Clone the project
* Run the Docker Terminal
* cd to the project local folder
* Check the docker machine ip (run in docker terminal: docker-machine ip), usually 192.168.99.100
* /bin/bash chat.sh
* Open <docker machine ip>:8080 in browser

### About ###

* Repo owner: Iurii Zagarskii