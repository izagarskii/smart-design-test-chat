#!/bin/bash
echo "1. Building maven project..."
mvn clean package -DskipTests
echo "Maven project built"
echo "2. Creating target/dependency directory and setting it up..."
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
echo "target/dependency directory is created and set up"
echo "3. Building docker image"
docker-compose up --build
exit 0