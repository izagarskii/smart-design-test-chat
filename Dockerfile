FROM openjdk:8-jdk-alpine
RUN addgroup -S chat-test && adduser -S chat-test -G chat-test
USER chat-test:chat-test
ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","com.zagarskii.chat.ChatApplication"]