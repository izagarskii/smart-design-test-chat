package com.zagarskii.chat.listeners;

import com.zagarskii.chat.config.WebSocketConfig;
import com.zagarskii.chat.domain.ChatMessage;
import com.zagarskii.chat.domain.MessageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import static com.zagarskii.chat.domain.MessageType.*;

/**
 * WebSocket events listener
 */
@Component
public class ChatEventListener {

    /**
     * User leave message template
     */
    private static final String leaveStringTemplate = "%s has left the chat";

    /**
     * Stomp messaging template
     */
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    /**
     * Listens for connect events
     * @param event SessionConnectedEvent - connection event
     */
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        System.out.println("Received a new websocket connection");
    }

    /**
     * Listens for disconnect events and sends 'user left chat room' message to the client
     * @param event SessionDisconnectEvent - disconnect event
     */
    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");
        if (username != null) {
            ChatMessage message = new ChatMessage();
            message.setUsername(username);
            message.setType(LEAVE.getValue());
            message.setContent(String.format(leaveStringTemplate, username));

            messagingTemplate.convertAndSend(WebSocketConfig.MESSAGES_TOPIC, message);
        }
    }
}
