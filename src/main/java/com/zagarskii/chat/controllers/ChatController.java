package com.zagarskii.chat.controllers;

import com.zagarskii.chat.domain.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import static com.zagarskii.chat.config.WebSocketConfig.MESSAGES_TOPIC;

/**
 * Chat WebSocket topics controller
 */
@Controller
public class ChatController {

    /**
     * Maps incoming messages to message topic
     * @param chatMessage - ChatMessage, websocket message payload
     * @return ChatMessage
     */
    @MessageMapping("/chat.send")
    @SendTo(MESSAGES_TOPIC)
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

    /**
     * Maps incoming login events to message topic
     * @param chatMessage - ChatMessage, websocket message payload
     * @param headerAccessor - SimpMessageHeaderAccessor, websocket message headers
     * @return ChatMessage
     */
    @MessageMapping("/chat.login")
    @SendTo(MESSAGES_TOPIC)
    public ChatMessage login(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getUsername());
        return chatMessage;
    }
}
