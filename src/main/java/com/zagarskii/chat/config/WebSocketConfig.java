package com.zagarskii.chat.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Application WebSocket configuration
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * Main messages topic
     */
    public static final String MESSAGES_TOPIC = "/topic/room";

    /**
     * Registers application stomp endpoint for messaging
     * @param registry - StompEndpointRegistry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/chat-stomp").withSockJS();
    }

    /**
     * Configures RabbitMQ stomp broker connection
     * @param registry - MessageBrokerRegistry
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app");
        registry.enableStompBrokerRelay("/topic").setRelayHost("rabbitmq")
                .setRelayPort(61613).setClientLogin("guest").setClientPasscode("guest");

    }
}
