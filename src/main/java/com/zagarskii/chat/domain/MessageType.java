package com.zagarskii.chat.domain;

public enum MessageType {

    ENTER("enter"),
    LEAVE("leave"),
    MESSAGE("message");

    private String value;

    MessageType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
