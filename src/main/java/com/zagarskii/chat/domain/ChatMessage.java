package com.zagarskii.chat.domain;

/**
 * WebSocket message domain object
 */
public class ChatMessage {

    /**
     * Users's name
     */
    private String username;

    /**
     * Message content
     */
    private String content;

    /**
     * Message type
     */
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
