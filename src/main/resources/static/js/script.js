'use strict';
document.querySelector('#welcomeForm').addEventListener('submit', connect, true)
document.querySelector('#dialogueForm').addEventListener('submit', sendMessage, true)
document.querySelector('#leaveForm').addEventListener('submit', disconnect, true)
var stompClient = null;
var name = null;

// Enter chat button callback event; performs backend connection via stomp client
function connect(event) {
	name = document.querySelector('#name').value.trim();
	if (name) {
		document.querySelector('#welcome-page').classList.add('hidden');
		document.querySelector('#dialogue-page').classList.remove('hidden');
		var socket = new SockJS('/chat-stomp');
		stompClient = Stomp.over(socket);
		stompClient.connect({}, connectionSuccess);
	}
	event.preventDefault();
}

// Leave chat button callback event
function disconnect(event) {
    stompClient.disconnect(disconnectSuccess, {});
}

// The callback event for successful connect action; sends 'user enters room' message
function connectionSuccess() {
	stompClient.subscribe('/topic/room', onMessageReceived);
	stompClient.send("/app/chat.login", {}, JSON.stringify({
		username : name,
		type: "enter"
	}))
}

// The callback event for successful diconnect; sends 'user leaves room' message
function disconnectSuccess() {
        document.querySelector('#welcome-page').classList.remove('hidden');
        document.querySelector('#dialogue-page').classList.add('hidden');
}

// Creates message and sends it via stomp client
function sendMessage(event) {
	var messageContent = document.querySelector('#chatMessage').value.trim();
	if (messageContent && stompClient) {
		var chatMessage = {
			username : name,
			content : document.querySelector('#chatMessage').value
		};
		stompClient.send("/app/chat.send", {}, JSON
				.stringify(chatMessage));
		document.querySelector('#chatMessage').value = '';
	}
	event.preventDefault();
}

// Incoming stomp messages handler; parses the incoming JSON and creates a DOM element according message type
function onMessageReceived(payload) {
	var message = JSON.parse(payload.body);
	var messageElement = document.createElement('li');
	if (message.type === 'enter') {
		messageElement.classList.add('event-data');
		message.content = message.username + ' has joined the chat';
	} else if (message.type === 'leave') {
		messageElement.classList.add('event-data');
		message.content = message.username + ' has left the chat';
	} else {
		messageElement.classList.add('message-data');
		var usernameElement = document.createElement('span');
		var usernameText = document.createTextNode(message.username);
		usernameElement.appendChild(usernameText);
		messageElement.appendChild(usernameElement);
	}
	var textElement = document.createElement('p');
	var messageText = document.createTextNode(message.content);
	textElement.appendChild(messageText);
	messageElement.appendChild(textElement);
	document.querySelector('#messageList').appendChild(messageElement);
	document.querySelector('#messageList').scrollTop = document
			.querySelector('#messageList').scrollHeight;
}